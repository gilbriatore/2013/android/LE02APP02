package br.up.edu.le02app02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void mostrarCorVermelha(View v){
        Toast.makeText(this, "Cor Vermelha", Toast.LENGTH_LONG).show();
    }

    public void mostrarCorAmarela(View v){
        Toast.makeText(this, "Cor Amarela", Toast.LENGTH_LONG).show();
    }

    public void mostrarCorVerde(View v){
        Toast.makeText(this, "Cor Verde", Toast.LENGTH_LONG).show();
    }

    public void mostrarCorAzul(View v){
        Toast.makeText(this, "Cor Azul", Toast.LENGTH_LONG).show();
    }


}
